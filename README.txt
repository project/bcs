Block Content Selected

With this module you can easily select the nodes to be displayed in
current block with autocomplete and also the choice of view mode.

If you want to customize the display of the content of block,
just overload the block with the use of variable
content['#bcs_items'] in the file of the current block template.
